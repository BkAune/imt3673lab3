package com.example.ballthrowgame_imt3673_lab3;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;
import static java.lang.Math.sqrt;

public class MainActivity extends AppCompatActivity implements SensorEventListener
{
    //Static values
    final static float EARTH_GRAVITY = -9.81f;
    final static int EVENTS_POLLED = 20;

    //Preferences helper object
    Preferences preferences;

    //cutoff point for acceleration readings
    float cutOff;

    //Stores local sensors and sensor management
    SensorManager sensorManager;
    Sensor accelerometer;

    //Accelerometer readings along corresponding axis
    double ax, ay, az;

    //UI button elements
    Button btnPreferences, btnReset;

    //UI text elements
    TextView distanceText, speedText, timeText;

    //Values for logic
    boolean thrown, calculating;
    float[] accelerations; //Array for storing
    int amountOfReadings; //counter variable for registering acceleration readings

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUp();

    }

    /**
     * Empty override function, not in use
     */
    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1){
        //Auto-generated method stub
    }

    /**
     * @param event SensorEvent
     *              Runs one of four responses to the event.
     *              First is to start a throw
     *              Second is to continue registering the throw
     *              Third is to run the calculation based on sensor input data.
     *              Fourth is running no calculation
     */
    @Override
    public void onSensorChanged(SensorEvent event)
    {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
        {
            if(!thrown)
            {
                ax = event.values[0];
                ay = event.values[1];
                az = event.values[2];
                if (calcAcceleration() > cutOff) {
                    registerAcceleration(event);
                    thrown = true;
                }
            }
            else if (thrown && amountOfReadings < EVENTS_POLLED)
            {
                registerAcceleration(event);
               // distanceText.setText("Adding");
            }
            else if (thrown && amountOfReadings == EVENTS_POLLED && !calculating)
            {
                calculating = true;
                throwBall();
            }
        }
    }

    /**
     * Runs all physics calculation on the ball.
     * Updates UI with correct new values.
     * Instead of counting the distance up over the time it will take the ball to travel to it's highest point.
     * We get this by running the following formulas
     * Travel time is calculated using the formula t=(v(start) - v(end))/a.
     * This gives us a value t for the formula Distance = v(start)*time + (1/2)acceleration*time^2.
     */
    private void throwBall()
    {
        float travelTime;
        float startSpeed = accelerations[highestAcceleration()];
        //
        travelTime = (0-startSpeed) / EARTH_GRAVITY;

        float distance = startSpeed*travelTime + 0.5f*EARTH_GRAVITY*travelTime*travelTime;
        distanceText.setText(Float.toString(distance));
        speedText.setText(Float.toString(startSpeed));
        timeText.setText(Float.toString(travelTime));
        btnReset.setEnabled(true);

        //updates highscore if new throw is higher than highest throw
        if(distance > preferences.getHighScoreValue())
        {
            preferences.updateHighScoreValue(distance);
        }
    }

    /**
     * @param event SensorEvent. Generates values added to acceleration array
     */
    private void registerAcceleration(@NotNull SensorEvent event) {
        ax = event.values[0];
        ay = event.values[1];
        az = event.values[2];
        accelerations[amountOfReadings] = calcAcceleration();
        amountOfReadings++;
    }

    /**
     * @return cross product of acceleration minus earth's gravity.
     */
    public float calcAcceleration()
    {
        return (float)(sqrt(ax*ax + ay*ay + az+az) + EARTH_GRAVITY);
    }


    /**
     * Helper function to set up the initial values for the activity
     */
    private void setUp()
    {
        //Loading values from preferences
        preferences = new Preferences((getApplicationContext()));
        cutOff = preferences.getCutOffValue();

        //Initializing Sensor manager and related listeners
        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        //initializing UI text elements. Setting text to default 0 value.
        distanceText = findViewById(R.id.txt_distance);
        speedText = findViewById(R.id.txt_speed);
        timeText = findViewById(R.id.txt_traveltime);
        timeText.setText("0");
        distanceText.setText("0");
        speedText.setText("0");

        //Initializing UI button elements with corresponding listeners
        btnReset = findViewById(R.id.btn_reset);
        btnReset.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                reset();
            }
        });
        btnReset.setEnabled(false);
        btnPreferences = findViewById(R.id.btn_pref);
        btnPreferences.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent preferencesIntent;
                preferencesIntent = new Intent(getApplicationContext(), PreferenceActivity.class);
                startActivity(preferencesIntent);
            }
        });

        //Setting private variables default values.
        thrown = false;
        calculating = false;
        accelerations = new float[EVENTS_POLLED];
        amountOfReadings = 0;
    }

    /**
     * @return array position of highest acceleration
     */
    public int highestAcceleration()
    {
        float high = 0.f;
        int position = -1;

        for(int i = 0; i < EVENTS_POLLED; i++)
        {
            if(accelerations[i] > high)
            {
                position = i;
            }
        }
        return position;
    }

    /**
     * Resets throw, let's you throw again
     */
    private void reset()
    {
        thrown =false;
        calculating = false;
        amountOfReadings = 0;
        timeText.setText("0");
        distanceText.setText("0");
        speedText.setText("0");
        btnReset.setEnabled(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        cutOff = preferences.getCutOffValue();
    }
}