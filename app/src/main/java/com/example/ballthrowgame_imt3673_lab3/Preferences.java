package com.example.ballthrowgame_imt3673_lab3;

import android.content.SharedPreferences;
import android.content.Context;

//Wrapper class for this projects use of Shared Preferences
public class Preferences
{
    //Internal constant values
    private static final String FILENAME = "Preferences";
    private static final String CUTOFF_VALUE = "CutOffValue";
    private static final String HIGHSCORE_VALUE = "HighScoreValue";

    //Storing class instance of SharedPreferences
    SharedPreferences sharedPreferences;

    /***
     *
     * @param context Application context
     *                Overloaded constructor taking in context from application to initiate SharedPreferences
     */
    public Preferences(@org.jetbrains.annotations.NotNull Context context)
    {
        sharedPreferences = context.getApplicationContext().getSharedPreferences(FILENAME,0);
    }

    /**
     *
     * @return stored cutoff value
     */
    public float getCutOffValue()
    {
        return sharedPreferences.getFloat(CUTOFF_VALUE, 5.0f);
    }

    /**
     *
     * @param f Sets stored value equal to f
     */
    public void updateCutOffValue(float f)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(CUTOFF_VALUE, f);
        editor.commit();
    }

    /**
     *
     * @return Stored highscore value
     */
    public float getHighScoreValue()
    {
        return sharedPreferences.getFloat(HIGHSCORE_VALUE, 0);
    }

    /**
     *
     * @param f Updates stored highscore to be equal to f
     */
    public void updateHighScoreValue(float f)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(HIGHSCORE_VALUE, f);
        editor.commit();
    }
}
