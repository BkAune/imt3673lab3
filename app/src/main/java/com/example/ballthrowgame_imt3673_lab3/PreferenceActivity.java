package com.example.ballthrowgame_imt3673_lab3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class PreferenceActivity extends AppCompatActivity
{
    Preferences preferences;
    float cutOff;
    SeekBar seekBar;
    TextView barValue;
    TextView bestThrow;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preferences_activity);

        //Initialzing Preferenes object and getting cutoff value from it
        preferences = new Preferences(getApplicationContext());
        cutOff = preferences.getCutOffValue();

        //Initializing progressbar as discreet seek bar. Sets the starting value
        seekBar = findViewById(R.id.bar_preferences);
        seekBar.setProgress((int)cutOff);

        //Initializing listeners
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                cutOff = (float) seekBar.getProgress();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                // TODO Auto-generated method stub
                barValue.setText(Integer.toString(seekBar.getProgress()));

            }
        });

        //Initializing UI text elements
        barValue = findViewById(R.id.txt_barvalue);
        barValue.setText(Float.toString((int)cutOff));
        bestThrow = findViewById(R.id.txt_bestthrow);
        bestThrow.setText(Float.toString(preferences.getHighScoreValue()));
    }

    /**
     * Extra behavior on back button pressed.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        preferences.updateCutOffValue(cutOff);
    }
}
